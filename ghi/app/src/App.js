import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './Inventory/ManufacturersList'
import ManufacturerForm from './Inventory/ManufacturerForm'
import VehicleModelsList from './Inventory/VehicleModelsList'
import VehicleModelForm from './Inventory/VehicleModelForm'
import AutosList from './Inventory/AutosList'
import AutoForm from './Inventory/AutoForm'
import ManufacturersList2 from './Inventory/ManufacturersList2'
import AutoList2 from './Inventory/AutoList2'
import VehicleModelList2 from './Inventory/VehicleModelList2'
import SalesPersonList from './Sales/SalesPersonList'
import CustomerList from './Sales/CustomerList'
import SalesList from './Sales/SalesList'
import SalesPersonForm from './Sales/SalesPersonForm'
import CustomerForm from './Sales/CustomerForm'
import SalesForm from './Sales/SalesForm'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        {/*
        <ManufacturersList manufacturers={props.manufacturers}/>
        <VehicleModelsList models={props.models}/>
        <AutosList autos={props.autos}/>
        */}
        <SalesForm/>


        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
