from django.shortcuts import render
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Technician, Appointments, Inventory_vinVO

class Inventory_vinVOEncoder(ModelEncoder):
    model = Inventory_vinVO
    properties = [
        "inventory_vin",
    ]

class TechniciansListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]

class AppointmentsListEncoder(ModelEncoder):
    model = Appointments
    properties = [
        "vin",
        "customer_name",
        "date",
        "technician_name",
        "reason",
        "in_inventory",
    ]

# Create your views here.

def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechniciansListEncoder
            )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder=TechniciansListEncoder,
            safe=False
        )

def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointments.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentsListEncoder
        )
    else:
        content = json.loads(request.body)
        appointments = Appointments.objects.create(**content)
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentsListEncoder,
            safe=False
        )

def api_list_VINVO(request):
    if request.method == "GET":
        vin = Inventory_vinVO.objects.all()
        return JsonResponse(
            {"vin":vin},
            encoder=Inventory_vinVOEncoder
        )
    else:
        content = json.loads(request.body)
        vin = Inventory_vinVO.objects.create(**content)
        return JsonResponse(
            {"vin":vin},
            encoder = Inventory_vinVOEncoder
        )
