from django.db import models
from django.urls import reverse

# Create your models here.

class Inventory_vinVO(models.Model):
    inventory_vin = models.CharField(max_length=17)

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.IntegerField(unique=True)

class Appointments(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=100)
    date = models.DateTimeField()
    technician_name = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE
        )
    reason = models.CharField(max_length=200)
    inventory_vin = models.ForeignKey(
        Inventory_vinVO,
        on_delete=models.PROTECT,
        )
    in_inventory = models.BooleanField()
